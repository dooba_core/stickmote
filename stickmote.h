/* Dooba SDK
 * Library for the Stickmote
 */

#ifndef	__STICKMOTE_H
#define	__STICKMOTE_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <pwm/pwm.h>

// WiFi Enable Pin
#define	STICKMOTE_WIFI_EN							10

// Thumbstick pins
#define	STICKMOTE_STICK_X							0
#define	STICKMOTE_STICK_Y							1

// LED Pin
#define	STICKMOTE_LED								18

// LED Shortcuts
#define	stickmote_led(v)							pwm_set(STICKMOTE_LED, v)

// Pre-Initialize Stickmote
extern void stickmote_preinit();

#endif
