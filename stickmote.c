/* Dooba SDK
 * Library for the Stickmote
 */

// External Includes
#include <dio/dio.h>

// Internal Includes
#include "stickmote.h"

// Pre-Initialize Stickmote
void stickmote_preinit()
{
	// Enable WiFi
	dio_output(STICKMOTE_WIFI_EN);
	dio_hi(STICKMOTE_WIFI_EN);

	// Configure LED
	stickmote_led(0);
}
